<?php
$accion = isset($_GET['accion']) ? $_GET['accion'] : '';
if ($accion === "enviar") {
    $nombre   = $_POST['nombre'];
    $email    = $_POST['email'];
    $objetivo   = $_POST['objetivo'];
    $industria    = $_POST['industria'];
    $comentarios = $_POST['comentarios'];
    
    $to        = 'lupokemontes@gmail.com';
    $asunto    = "Nuevo mensaje desde uakika.com.ar!";
    $contenido = "Nombre: " . $nombre . "<br>";
    $contenido .= "E-Mail: " . $email . "<br>";
    $contenido .= "Objetivo: " . $objetivo . "<br>";
    $contenido .= "Industria: " . $industria . "<br>";
    $contenido .= "Comentarios: " . $comentarios . "<br>";
    
    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $headers .= "X-Priority: 3\n";
    $headers .= "X-MSmail-Priority: Normal\n";
    $headers .= "X-mailer: php\n";
    $headers .= "From: Contacto <contacto@uakika.com.ar>\r\n";
    $headers .= "Reply-To: " . $email . "\r\n";
    $headers .= "Return-Path: " . $email . "\r\n";
    
    if (mail($to, $asunto, $contenido, $headers)) {
        header('Location: ?accion=enviado');
    }
    
    else {
        header('Location: ?accion=error');
    }
}
?>