<?php include 'library/common.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/fancybox.css">
<script src="js/vendor/modernizr-2.6.2.min.js"></script>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter:none;
    }
  </style>
<![endif]-->
</head>
<body class="espanol">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div id="inicio" class="clearfix">
    <div class="image shadow-inner-bottom"><iframe width="550" height="288" src="//www.youtube.com/embed/hhrsEjZRD38?rel=0" frameborder="0" allowfullscreen></iframe></div>
    <div class="wrapper center bg-white">
        <ul class="center t-center">
            <li><span class="icon-facebook1">&nbsp;</span></li>
            <li><span class="icon-mail">&nbsp;</span></li>
            <li><span class="icon-twitter1">&nbsp;</span></li>
            <li class="t-center">
                <h1 class="center">uakika NFC</h1>
                <h2 class="italic blue-light tx01">Vea más allá, vinculando el mundo digital y físico.</h2>
            </li>
            <li><span class="icon-twitter2">&nbsp;</span></li>
            <li><span class="icon-cart">&nbsp;</span></li>
            <li><span class="icon-facebook2">&nbsp;</span></li>
        </ul>
    </div>
</div>
<header class="shadow-outer-bottom bg-blue-light clearfix">
    <div class="wrapper center clearfix">
        <!--div class="left logo">Uakika NFC</div-->
        <nav class="clearfix">
            <a href="#inicio" class="item tx02 current">Inicio</a>
            <a href="#quienes-somos" class="item tx03">Quiénes Somos</a>
            <a href="#a-quien-esta-dirigido" class="item tx04">¿A quién está dirigido?</a>
            <a href="#como-funciona" class="item tx05">¿Cómo funciona?</a>
            <a href="#beneficios" class="item tx06">Beneficios</a>
            <a href="#faqs" class="item tx07">FAQs</a>
            <a href="#contacto" class="item tx08">Contacto</a>
            <div class="languaje clearfix">
            	<a href="index-english.php" class="es">&nbsp;</a>
	            <a href="index-english.php" class="en">&nbsp;</a>
	            <a href="index-english.php" class="br">&nbsp;</a>
	        </div>
            <div class="dropdown">
                <a href="#" class="login border-radius bg-blue-dark tx09">Log In</a>
                <form class="shadow-outer border-radius">
                    <!--span class="tx10">Mensaje de error</span-->
                    <input type="text" class="text border-box border-radius border-gray bg-white tx11" name="user" placeholder="Usuario">
                    <input type="text" class="text border-box border-radius border-gray bg-white tx12" name="password" placeholder="Contraseña">
                    <input type="submit" class="left submit shadow-outer border-radius bg-blue-gradient blue-lighter tx13" value="Ingresar">
                    <a href="#" class="right blue-dark tx14">Regístrese</a>
                    <a href="#" class="right blue-dark tx15">¿Olvidó su contraseña?</a>
                </form>
            </div>          
            <!--div class="onoffswitch">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                <label class="onoffswitch-label" for="myonoffswitch">
                    <div class="onoffswitch-inner"></div>
                    <div class="onoffswitch-switch"></div>
                </label>
            </div-->
        </nav>
    </div>
</header>
<div id="quienes-somos" class="clearfix">
    <div class="wrapper center">
        <h1 class="blue-light tx16">Somos Uakika</h1>
        <hr>
        <h5 class="italic tx17">Un start-up latinoamericano con base en Buenos Aires, Argentina. Un equipo de expertos y soñadores que son conscientes del gran potencial que hay en nuestra región. América Latina está convirtiéndose cada vez más en una realidad multiplataforma y en Uakika potenciamos la relación entre el mundo físico y el mundo digital mediante innovación tecnológica y creatividad.<br></h5>
    </div>
    <div class="bg-gray">
        <div class="wrapper center">
            <h1 class="blue-light tx18">Equipo Fundador</h1>
            <ul class="clearfix">
                <li class="left andres" style="margin-left:180px">
                    <div class="border-box italic t-center">
                        <h3 class="bg-blue-medium white">Andrés Kelly</h3>
                        <span class="tx19">Co-Founder</span></div>
                </li>
                <li class="left mauro">
                    <div class="border-box italic t-center">
                        <h3 class="bg-blue-medium white">Mauro Carrió</h3>
                        <span class="tx20">Co-Founder</span></div>
                </li>
                <!--li class="left alejandro">
                    <div class="border-box italic t-center">
                        <h3 class="bg-blue-medium white">Alejandro Gassman</h3>
                        <span class="tx21">CTO</span></div>
                </li>
                <li class="left gabriela">
                    <div class="border-box italic t-center">
                        <h3 class="bg-blue-medium white">Gabriela Fanzone</h3>
                        <span class="tx22">Creativa</span></div>
                </li-->
            </ul>
        </div>
    </div>
</div>
<div id="a-quien-esta-dirigido" class="t-center clearfix">
    <h1 class="wrapper center blue-light tx23">¿A quién está dirigido?</h1>
    <div class="bg-gray">
        <ul class="wrapper center clearfix">
            <li class="left border-box border-radius border-gray bg-white t-center eventos">
                <span class="shadow-outer border-white circle">&nbsp;</span>
                <h4 class="blue-medium tx24">Eventos</h4>
                <p class="italic tx25">Viralizamos su evento en las redes sociales a través de sus visitantes, brindándoles una forma divertida de vincularlos con sus redes sociales.</p>
            </li>
            <li class="right border-box border-radius border-gray bg-white t-center b2b">
                <span class="shadow-outer border-white circle">&nbsp;</span>
                <h4 class="blue-medium tx26">B2B</h4>
                <p class="italic tx27">Nuestra plataforma es lo suficientemente robusta y flexible para adaptarse a diversos escenarios y necesidades.</p>
            </li>
            <li class="border-box border-radius border-gray bg-white t-center marcas">
                <span class="shadow-outer border-white circle">&nbsp;</span>
                <h4 class="blue-medium tx28">Marcas</h4>
                <p class="italic tx29">Utilice nuestro <b>Programa de Fidelización</b>, para conocer a sus clientes y establecer las estrategias de markeing que le permitan potenciar su relación con ellos.</p>
            </li>
        </ul>
    </div>
    <h3 class="wrapper center shadow-outer-bottom-central border-white-bottom bg-gray blue-medium italic tx30">¿Tiene una idea? Estamos ansiosos por escucharla.</h3>
    <div class="border-blue-light-top border-blue-light-bottom bg-blue-gradient">
        <ul class="wrapper center clearfix">
            <li class="left border-box t-center eventos">
                <span class="shadow-outer border-white circle">&nbsp;</span>
                <h2><a href="#eventos-entretenimiento" class="block shadow-outer border-radius bg-white blue t-center fancybox tx">Eventos<br>y entretenimiento</a>
                </h2>
            </li>
            <li class="right border-box t-center viajes">
                <span class="shadow-outer border-white circle">&nbsp;</span>
                <h2><a href="#eventos-entretenimiento" class="block shadow-outer border-radius bg-white blue t-center fancybox tx">Viajes<br>y alojamiento</a>
                </h2>
            </li>
            <li class="border-box t-center marcas">
                <span class="shadow-outer border-white circle">&nbsp;</span>
                <h2><a href="#eventos-entretenimiento" class="block shadow-outer border-radius bg-white blue t-center fancybox tx">Marcas<br>y negocios</a>
                </h2>
            </li>
        </ul>
    </div>
</div>
<div id="como-funciona" class="t-center clearfix">
    <div class="wrapper center">
        <h1 class="blue-light tx32">¿Cómo funciona?</h1>
        <hr>
        <h4 class="italic tx33">Uakika NFC funciona en 3 simples pasos.</h4>
    </div>
    <div class="bg-gray">
        <ul class="wrapper center clearfix">
            <li class="left border-box t-center uno">
                <span class="image">&nbsp;</span>
                <h1 class="center shadow-outer border-white circle bg-blue-lighter white">1</h1>
                <p class="italic tx34">Los usuarios reciben la<br>identificación NFC personalizada<br>con la imagen de su empresa, negocio o evento.</p>
            </li>
            <li class="right border-box t-center tres">
                <span class="image">&nbsp;</span>
                <h1 class="center shadow-outer border-white circle bg-blue-lighter white">3</h1>
                <p class="italic tx35">Interactúan con los puntos de lectura, sumando puntos y participando de increíbles sorteos y premios.</p>
            </li>
            <li class="border-box t-center dos">
                <span class="image">&nbsp;</span>
                <h1 class="center shadow-outer border-white circle bg-blue-lighter white">2</h1>
                <p class="italic tx36">Se registran con sus redes sociales (Facebook, Linkedin, Twitter, etc.) o completando el formulario que usted establezca.</p>
            </li>
        </ul>
    </div>
    <h1 class="border-blue-light-top border-blue-light-bottom bg-blue-gradient white t-center text-shadow tx37">Establezca <span class="blue-light">las acciones ¡que se le ocurran!</span> que se dispararán<br>cuando el usuario se acerque a un punto de lectura.</h1>
</div>
<div id="beneficios" class="t-center clearfix">
    <h1 class="wrapper center blue-light tx38">Beneficios</h1>
    <ul class="wrapper center clearfix">
        <li class="left">
            <div class="border-gray-top border-gray-bottom"><span class="audiencia">&nbsp;</span>
                <h4 class="blue-medium tx39">Conozca su audiencia</h4>
                <p class="italic tx40">La audiencia es más que un número. Utilice sus datos para poder conocer con mayor detalle, cuales son sus gustos, caraterísticas, preferencias, etc.</p>
            </div>
            <div class="border-gray-bottom">
                <span class="viralice">&nbsp;</span>
                <h4 class="blue-medium tx41">Viralice las acciones</h4>
                <p class="italic tx42">Cada uno de los visitantes es un divulgador de su mensaje por todas las redes sociales viralizando su presencia. Además puede customizar acciones como compras, pagos, envío de información, conectar con alguien, etc.</p>
            </div>
        </li>
        <li class="right">
            <div class="border-gray-top border-gray-bottom">
                <span class="influya">&nbsp;</span>
                <h4 class="blue-medium tx43">Influya en la audiencia</h4>
                <p class="italic tx44">Cada interacción en el mundo físico, se ve reflejado en el digital potenciando la llegada a nuevos clientes de nuestros mensajes, promociones, etc.</p>
            </div>
            <div class="border-gray-bottom">
                <span class="reportes">&nbsp;</span>
                <h4 class="blue-medium tx45">Reportes</h4>
                <p class="italic tx46">Uakika NFC permite visualizar todos los datos relacionado con los usuarios, brindando una información que antes era imposible de recopilar y analizar.</p>
            </div>
        </li>
        <li>
            <div class="border-gray-top border-gray-bottom"><span class="gamification">&nbsp;</span>
                <h4 class="blue-medium tx47">Gamification</h4>
                <p class="italic tx48">Incentive a sus visitantes, permitiendo que sumen puntos para canjear por premios de su catálogo, participen de increíbles sorteos, compitan por ser los primeros en la tabla de posiciones, etc.</p>
            </div>
            <div class="border-gray-bottom">
                <span class="control">&nbsp;</span>
                <h4 class="blue-medium tx49">Control de acceso y VIP</h4>
                <p class="italic tx50">¿Necesita agilizar el ingreso? ¿Asegurar la identidad de la persona? ¿Evitar entradas falsas? Uakika NFC puede ayudarlo con la última tecnología.</p>
            </div>
        </li>
    </ul>
    <h1 class="border-blue-light-top border-blue-light-bottom bg-blue-gradient white t-center text-shadow tx51">Configure el <span class="blue-light">catálogo de premios</span> o los sorteos<br>con los que incentivará a sus clientes, fans o visitantes<br>a realizar las acciones.</h1>
</div>
<div id="faqs" class="clearfix">
    <h1 class="wrapper center border-gray-bottom blue-light t-center tx52">FAQs</h1>
    <ul class="wrapper center clearfix">
        <li class="border-gray-bottom">
            <h4 class="blue-lighter tx53">¿Qué es NFC?</h4>
            <p class="italic tx54">NFC es la abreviatura de Near Field Communication, tecnología de Identificación por Radio Frecuencia (RFID) en 13,56 Mhz. Para la captura del dato la tecnología RFID consta de dos elementos: los tags (en sus diferentes formatos como pulsera, llavero, tarjeta, etc) que contienen la identificación unívoca del mismo y los lectores (puntos de lectura) mediante los cuales se emiten las ondas de Radio Frecuencia, activan los tags y reciben los datos transmitidos.</p>
        </li>
        <li class="border-gray-bottom">
            <h4 class="blue-lighter tx55">¿Qué es Gamification?</h4>
            <p class="italic tx56">Gamification es el uso de elementos y técnicas de juego en contextos no-lúdicos con el objetivo de incentivar a los clientes, fans y visitantes a realizar las acciones que consideremos apropiadas.</p>
        </li>
        <li class="border-gray-bottom">
            <h4 class="blue-lighter tx57">¿Qué es Uakika NFC?</h4>
            <p class="italic tx58">Uakika NFC es la plataforma que, a través de la tecnología NFC y técnicas de Gamification, le permitirá brindar una experiencia a sus usuarios, fans y visitantes, motivándolos a realizar las acciones que usted establezca como prioritarias.</p>
        </li>
        <li class="border-gray-bottom">
            <h4 class="blue-lighter tx59">¿La solución incluye hardware?</h4>
            <p class="italic tx60">Podemos proveer el hardware, contactarlo con algunos de nuestros socios de negocios o bien desarrollar hardware a medida, dependiendo de sus necesidades.</p>
        </li>
        <li class="border-gray-bottom">
            <h4 class="blue-lighter tx61">¿El servicio está disponible en otros países?</h4>
            <p class="italic tx62">Uakika NFC está disponible en cualquier lugar que tenga una conexión de Internet y la posibilidad de hacer llegar los equipos de hardware.</p>
        </li>
        <li class="border-gray-bottom">
            <h4 class="blue-lighter tx63">Tengo una idea, ¿me pueden ayudar?</h4>
            <p class="italic tx64">En Uakika NFC, nos encantan los desafíos y estamos ansiosos por escuchar su idea. En conjunto, podremos armar la solución que genere el mayor impacto para su  negocio.</p>
        </li>
    </ul>
</div>
<div id="contacto" class="clearfix">
    <h1 class="wrapper center blue-light tx65">Contacto</h1>
    <div class="left bg-gray clearfix">
        <form class="right clearfix" method="post" action="?accion=enviar" onSubmit="return validar(this);">
            <label for="nombre" class="left italic tx66">Nombre</label>
            <input type="text" class="text right border-box border-radius border-gray bg-white" name="nombre">
            <label for="email" class="left italic tx67">E-mail</label>
            <input type="text" class="text right border-box border-radius border-gray bg-white" name="email">
            <label for="objetivo" class="left italic tx68">Objetivo</label>
            <select name="objetivo" class="right border-box border-radius border-gray bg-white">
                <option value="1" class="tx69">Tengo un proyecto específico</option>
                <option value="2" class="tx70">Me gustaría vender Uakika NFC</option>
                <option value="3" class="tx71">Necesito más información</option>
            </select>
            <label for="industria" class="left italic tx72">Industria</label>
            <select name="industria" class="right border-box border-radius border-gray bg-white">
                <option value="1" class="tx73">Agencias y marcas</option>
                <option value="2" class="tx74">Desarrollo de eventos</option>
                <option value="3" class="tx75">Comercio</option>
                <option value="4" class="tx76">Otro</option>
            </select>
            <label for="comentarios" class="left italic tx77">Comentarios</label>
            <textarea name="comentarios" class="right border-box border-radius border-gray bg-white"></textarea>
            <input type="submit" class="submit clear right shadow-outer border-radius bg-blue-gradient blue-lighter tx78" value="ENVIAR">
        </form>
    </div>
    <div class="right">
        <div class="left t-center">
            <p>
                <a href="mailto:info@uakika.com">info@uakika.com</a>
            </p>
            <div class="shadow-outer-bottom-central-mini border-white-bottom bg-gray clearfix">
                <p class="left"><span class="argentina">&nbsp;</span>[54 11] 5258 2340</p>
                <p class="right"><span class="espana">&nbsp;</span>[34 91] 123 8507</p>
            </div>
            <a href="http://www.facebook.com/uakika" class="facebook">&nbsp;</a>
            <a href="http://www.twitter.com/uakika" class="twitter">&nbsp;</a>
        </div>
    </div>
</div>
<footer class="bg-blue-lighter">
    <ul class="wrapper center t-center clearfix">
        <li class="left italic tx79">Copyright © 2014 Uakika - Todos los derechos reservados</li>
        <li class="right italic t-right">
            <a href="terminos-de-uso.html" data-fancybox-type="iframe" class="fancybox tx80">Términos de uso</a>
            •
            <a href="privacidad.html" data-fancybox-type="iframe" class="fancybox tx81">Privacidad</a>
        </li>
        <li class="om">&nbsp;</li>
    </ul>
</footer>
    <div id="eventos-entretenimiento" class="aplicaciones" style="width:600px;display:none;">
        <h1 class="white tx16">Eventos y entretenimiento</h1>
        <div class="section clearfix">
            <h2>Control de Acceso por RFID</h2>
            <img src="img/control-de-acceso-por-rfid.png" class="left">
            <p>UakikaNFC le brinda una manera, probada, segura y dinámica de administrar el control de acceso a su evento.</p>
            <p>Gracias a las tarjetas, pulseras, llaveros y a los puntos de lectura Uakika (ya sea en formato de kiosco o mobile) podrá administrar de forma precisa y segura los puntos de accesos de todo su público eliminando en un 100% la falsificación de sus entradas.</p>
            <p>Usted tendrá toda la información de manera online y podrá a su vez controlar el acceso a otras instalaciones dentro de su evento como salones VIP, backstage, acceso restringido, etc.</p>
        </div>
        <div class="section clearfix">
            <h2>Integración con Redes Sociales</h2>
            <img src="img/integracion-con-redes-sociales.png" class="right">
            <p>La oportunidad de conectar nuestro público con las redes sociales, siendo ellos los que transmiten nuestros mensajes, nos permite alcanzar límites inimaginados.</p>
            <p>Nuestro público podrá, a través de los puntos de lectura Uakika NFC, realizar check-in, actualizar su estado y postear una foto en Facebook, twittear nuestro mensaje, compartir foto en Instagram, etc.</p>
            <p>Nuestro mensaje será transmitido de forma inmediata por toda las redes gracias a su público, permitiendo mantener contacto antes, durante y después del evento.</p>
        </div>
        <div class="section clearfix">
            <h2>Conozca su Audiencia</h2>
            <img src="img/conozca-su-audiencia.png" class="left">
            <p>Uakika NFC le permite conocer a su audiencia como nunca había pensado gracias a sus herramientas de recopilación y análisis de información. Imagine si usted puede saber cuáles son las bandas favoritas, que marca de cerveza prefieren, la red social que utiliza su audiencia, abriendo una cantidad infinita de beneficios a explotar.</p>
            <p>Formule las encuestas que desee realizar de forma dinámica y permita que su audiencia las complete en los puntos de lectura (kiosco y mobile) Uakika NFC, obteniendo los resultados de las mismas de forma inmediata.</p>
        </div>
        <div class="section clearfix">
            <h2>Programa de Fidelización y Sorteos</h2>
            <img src="img/programa-de-fidelización-y-sorteos.png" class="right">
            <p>Recompense a su audiencia por las acciones que realizan con increíbles premios.</p>
            <p>Configure el catálogo de premios con los que premiará a los clientes, estableciendo el stock, límite por usuario y cantidad de puntos necesarios para canjearlos. Su público podrá acumular puntos tras realizar sus acciones y canjearlos cuando haya llegado a la cantidad necesaria.</p>
            <p>Realice sorteos de forma transparente y dinámica a través de nuestra plataforma.</p>
        </div>
        <div class="section clearfix">
            <h2>Lanzamiento de Productos y Marcas</h2>
            <img src="img/lanzamiento-de-productos-y-marcas.png" class="left">
            <p>Uakika NFC permite conectar las marcas y productos con su audiencia de forma totalmente innovadora a través de la tecnología.</p>
            <p>El registro, vinculación y acciones en redes sociales, las encuestas y análisis de información abren nuevas posibilidad a las marcas de relacionarse e incentivar a los asistentes.</p>
        </div>
        <div class="section">
            <h2>API Web y Mobile</h2>
            <img src="img/api-web-y-mobile.png" class="right">
            <p>Sabemos que vivimos en un mundo multiplataforma. Por eso tenemos a disposición una API robusta para que pueda premiar cualquier acción que se realice en otras plataformas o sistemas.</p>
        </div>
    </div>
    <div id="viajes-alojamiento" class="aplicaciones" style="width:600px;display:none;">
        <h1 class="white tx16">Viajes y alojamiento</h1>
        <div class="section clearfix">
            <h2>Integración con Redes Sociales</h2>
            <img src="img/integracion-con-redes-sociales.png" class="left">
            <p>Los viajes y vacaciones son los momentos que más nos interesa compartir a través de las redes sociales y Uakika NFC nos brinda la posibilidad de conectar nuestros clientes con sus redes sociales, convirtiéndolos en divulgadores de nuestro mensaje por todo el mundo.</p>
            <p>Nuestro público podrá, a través de los puntos de lectura Uakika NFC, realizar check-in, actualizar su estado y postear una foto en Facebook, twittear nuestro mensaje, compartir foto en Instagram, etc.</p>
            <p>Su mensaje será transmitido de forma inmediata por toda las redes sociales gracias a sus clientes, permitiendo mantener contacto antes, durante y después de su estadía.</p>
        </div>
        <div class="section clearfix">
            <h2>Check-in y Control de Acceso por RFID</h2>
            <img src="img/control-de-acceso-por-rfid.png" class="right">
            <p>Permita que sus clientes realicen el check-in a través de los kioscos UakikaNFC completando el formulario de ingreso y obteniendo las tarjetas, pulseras, llaveros, etc. que los identificará y dará acceso a las instalaciones y servicios.</p>
            <p>Usted tendrá toda la información de manera online y podrá a su vez controlar el acceso a otras instalaciones dentro de sus instalaciones como restaurantes, zonas con acceso restringido, habitaciones, instalaciones recreativas, etc.</p>
        </div>
        <div class="section clearfix">
            <h2>Conozca su Audiencia</h2>
            <img src="img/conozca-su-audiencia.png" class="left">
            <p>Con Uakika NFC usted podrá conocer en detalle a sus clientes a través de los datos recopilados a través de su registro y redes sociales así como de las encuestas que usted puede realizar sobre la temática que considere apropiada, las cuales podrán ser completadas desde los puntos de lectura (kiosco y mobile) Uakika NFC, obteniendo los resultados y análisis de información de las mismas de forma inmediata.</p>
        </div>
        <div class="section clearfix">
            <h2>Programa de Fidelización y Sorteos</h2>
            <img src="img/programa-de-fidelización-y-sorteos.png" class="right">
            <p>Implemente un Programa de Fidelización de forma sencilla con Uakika y establezca las acciones con las que desea incentivar a sus clientes brindándoles puntos que serán canjeados por el catálogo de premios que usted establezca.</p>
            <p>Con Uakika NFC usted podrá, además de recompensar a sus clientes por las noches que se aloje, premiar con puntos cualquier acción que desee.</p>
            <p>Uakika también le permite novedosos Sorteos con los que premiar a sus clientes.</p>
        </div>
        <div class="section">
            <h2>API Web y Mobile</h2>
            <img src="img/api-web-y-mobile.png" class="right">
            <p>Sabemos que vivimos en un mundo multiplataforma. Por eso tenemos a disposición una API robusta para que pueda premiar cualquier acción que se realice en otras plataformas o sistemas.</p>
        </div>
    </div>
    <div id="marcas-negocios" class="aplicaciones" style="width:600px;display:none;">
        <h1 class="white tx16">Marcas y negocios</h1>
        <div class="section clearfix">
            <h2>Programa de Fidelización y Sorteos</h2>
            <img src="img/programa-de-fidelización-y-sorteos.png" class="left">
            <p>Implemente un programa de fidelización totalmente novedoso, donde usted podrá recompensar a sus clientes no sólo por las compras que realice si no por CUALQUIER acción que usted desee.</p>
            <p>Configure el catálogo de premios con los que premiará a los clientes, estableciendo el stock, límite por usuario y cantidad de puntos necesarios para canjearlos.</p>
            <p>Además realice sorteos de manera dinámica y transparente con Uakika.</p>
        </div>
        <div class="section clearfix">
            <h2>Integración con Redes Sociales</h2>
            <img src="img/integracion-con-redes-sociales.png" class="right">
            <p>Premie a sus clientes por compartir sus mensajes en las redes sociales llegando con nuestro mensaje a una cantidad de personas hasta ahora totalmente inimaginable. ¿Se imagina a su cliente posteando un foto con su último modelo de ropa o zapatos? ¿O compartiendo una oferta para que la descarguen sus cientos de amigos?.</p>
            <p>Podrán, a través de los puntos de lectura Uakika NFC, realizar check-in, actualizar su estado y postear una foto en Facebook, twittear nuestro mensaje, compartir foto en Instagram, etc.</p>
            <p>Su mensaje será transmitido de forma inmediata por toda las redes gracias a su público.</p>
        </div>
        <div class="section clearfix">
            <h2>Conozca su Audiencia</h2>
            <img src="img/conozca-su-audiencia.png" class="left">
            <p>Configure las encuestas que desee realizar de forma dinámica.</p>
            <p>Desde sus oficinas podrá establecer las preguntas y tipos de respuesta, las cuales serán sincronizadas de forma instantánea en los puntos de lectura Uakika NFC (sin importar el lugar geográfico donde se encuentren los mismos).</p>
            <p>Su público podrá completar la encuesta y ganar puntos por ello. Usted recibirá de forma inmediata los resultados pudiendo analizar toda la información y realizar las acciones que considere apropiadas.</p>
        </div>
        <div class="section clearfix">
            <h2>Lanzamiento de Productos y Marcas</h2>
            <img src="img/lanzamiento-de-productos-y-marcas.png" class="right">
            <p>El registro, vinculación y acciones en redes sociales, las encuestas y análisis de información abren nuevas posibilidad a las marcas de relacionarse e incentivar a los clientes actuales y potenciales de forma innovadora a través de la tecnología.</p>
        </div>
        <div class="section">
            <h2>API Web y Mobile</h2>
            <img src="img/api-web-y-mobile.png" class="left">
            <p>Sabemos que vivimos en un mundo multiplataforma. Por eso tenemos a disposición una API robusta para que pueda premiar cualquier acción que se realice en otras plataformas o sistemas.</p>
        </div>
    </div>

<!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script-->
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
<script src="js/vendor/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/vendor/jquery.fancybox.js"></script>
<script src="js/vendor/apprise-1.5.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function() {
<?php if ( $accion === "enviado" ) { ?>
apprise('Tu mensaje ha sido enviado exitosamente!');
<?php } if ( $accion === "error" ) { ?>
apprise('Ha habido un error al enviar tu mensaje! Por favor, int&eacute;ntalo nuevamente en unos minutos.');
<?php } ?>
});
</script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!--script>
(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
e=o.createElement(i);r=o.getElementsByTagName(i)[0];
e.src='//www.google-analytics.com/analytics.js';
r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
ga('create','UA-XXXXX-X');ga('send','pageview');
</script-->
</body>
</html>