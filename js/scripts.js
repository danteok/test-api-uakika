jQuery(function ($) {
    var uakCustomApi = new UakCustomApi();
// Signin
    $('#butSignin').click(function () {
        
        uakCustomApi = new UakCustomApi($('#customerClientKey').val());
        response = uakCustomApi.signUp($('#customerFanUserEmail').val());
        if (response.result) {
            //text = "accessToken: " + response.data.accessToken + '<br>';
            //text += "expiredAt: " + response.data.tokenExpiresAt.date;
            response = uakCustomApi.getUserInfo();
            if (response.result) {
                text = "<br>User Email : " + response.data.userEmail + '<br>';
                text += "User Name : " + response.data.userName + '<br>';
                text += "User Last Name : " + response.data.userLastName + '<br>';
            }
            else {
                text = "ERROR: " + response.error;
            }
        }
        else {
            console.log(response);
            text = "ERROR: " + response.error;
        }
        $('#response').html(text);
    });
// Add Points
    $('#comprar').click(function () {
        response = uakCustomApi.addPoints($('#actionHashCompra').val());
        //console.log(response);
        if (response.result) {
            text = "Se agregaron puntos por Comprar!";
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseComprar').html(text);
    });

    $('#comentar').click(function () {
        response = uakCustomApi.addPoints($('#actionHashComentar').val());
        //console.log(response);
        if (response.result) {
            text = "Se agregaron puntos por comentar!";
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseComentar').html(text);
    });
    $('.valoracion').click(function () {
        //alert('gracias por votar:' + $(this).attr('value'));
        response = uakCustomApi.addPoints($('#actionHashValorar').val());
        //console.log(response);
        if (response.result) {
            text = "Se agregaron puntos por Valorar!";
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseValorar').html(text);
    });
    $('#encuesta').click(function () {
        response = uakCustomApi.addPoints($('#actionHashEncuesta').val());
        //console.log(response);
        if (response.result) {
            text = "Se agregaron puntos por participar de Encuesta!";
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseEncuesta').html(text);
    });
    $('#newsletter').click(function () {
        response = uakCustomApi.addPoints($('#actionHashNewsletter').val());
        //console.log(response);
        if (response.result) {
            text = "Se agregaron puntos por suscribirse al Newsletter";
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseNewsletter').html(text);
    });
// Read Points
    $('#butReadPoints').click(function () {
        response = uakCustomApi.readPoints();
        //console.log(response);
        if (response.result) {
            text = "Total Puntos  : " + response.data.points;
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseReadPoints').html(text);
    });
// Action List
    $('#butActionList').click(function () {
        response = uakCustomApi.actionList();
        //console.log(response);
        if (response.result) {
            text = '';
            $.each(response.data.actions, function (index, action) {
                text += "<br>Action: " + action.actionTitle + ', Tag: ' + action.tag + ', fecha' + action.createdAt.date;
            });
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseActionList').html(text);
    });
// Leaderboard
    $('#butLeaderboard').click(function () {
        response = uakCustomApi.leaderboard();
        //console.log(response);
        if (response.result) {
            text = '';
            $.each(response.data.leaderboard, function (index, user) {
                text += '<br>(' + index + ") UserEmail: " + user.userEmail + ', Name: ' + user.userName + ' ' + user.userLastName + ', Points' + user.points;
            });
            if (response.neighbours) {
                if (response.neighbours.prev) {
                    user = response.neighbours.prev;
                    text += '<br>(PREV) UserEmail: ' + user.userEmail + ', Name: ' + user.userName + ' ' + user.userLastName + ', Points' + user.points;
                }
                if (response.neighbours.next) {
                    user = response.neighbours.next;
                    text += '<br>(NEXT) UserEmail: ' + user.userEmail + ', Name: ' + user.userName + ' ' + user.userLastName + ', Points' + user.points;
                }
            }
            else {
                text += '<br> no hay neighbours';
            }
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseLeaderboard').html(text);
    });
// Catalog
    $('#butCatalog').click(function () {
        response = uakCustomApi.catalog();
        //console.log(response);
        if (response.result) {
            text = '';
            $.each(response.data.rewards, function (index, reward) {
                text += "<br>Reward Id: " + reward.reward_id + ', Name: ' + reward.name + ', Points:' + reward.points;
            });
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseCatalog').html(text);
    });
// Redeem
    $('#butRedeem').click(function () {
        response = uakCustomApi.redeem($('#rewardId').val());
        console.log(response);
        if (response.result) {
            text = response.data.status;
        }
        else {
            text = "ERROR: " + response.error;
        }
        $('#responseRedeem').html(text);
    });
});