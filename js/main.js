//función para determinar la altura de la sección Inicio
function seccionInicio() {
	var windowHeight = $(window).height();
	var barHeight = $('#inicio .bg-white').outerHeight();
	//$('#inicio .image').height(windowHeight - barHeight);
}

//función para el funcionamiento del menú
var lastSecction = 'inicio';
function scrollWatch(section, nav) {
	var scrollTop = $(window).scrollTop()
	$(section).each(function (i, v) {
		if (scrollTop >= $('#' + v.id).offset().top - $(nav).height() - 50) {
			currentSection = $('#' + v.id).attr('id')
		}
	})
	if (lastSecction !== currentSection) {
		lastSecction = currentSection;
		$(nav + ' a.item').removeClass("current")
		$(nav + ' a.item[href="#' + currentSection + '"]').addClass('current');
	}
}
$('nav a:not(.login, .languaje a)').click(function (e) {
	e.preventDefault();
	var target = $(e.target).attr('href');
	var headerHeight = $('header').height();
	$('html, body').animate({
		scrollTop: $(target).offset().top - headerHeight
	}, 1000, 'easeInCubic');
});

//función para abrir el box de login
$('nav a.login').click(function (e) {
	e.preventDefault();
	$(this).toggleClass('opened');
});

// función para validar los datos ingresados en el formulario
function validar(form) {
	if (form.nombre.value == '') {
		apprise('Por favor, ingresa tu nombre!');
		form.nombre.focus();
		return false;
	}
	if (form.email.value == '') {
		apprise('Por favor, ingresa tu eMail!');
		form.email.focus();
		return false;
	}
	if ((form.email.value.indexOf('@') < 0) || ((form.email.value.charAt(form.email.value.length - 4) != '.') && (form.email.value.charAt(form.email.value.length - 3) != '.'))) {
		apprise('Has ingresado un eMail no válido! Por favor, inténtalo nuevamente.');
		form.email.focus();
		return false;
	}
	if (form.comentarios.value == "") {
		apprise('Por favor, ingresa tus comentarios!');
		form.mensaje.focus();
		return false;
	}
	return true;
}

//función para el switch de idiomas
function _callAjax(lang){
	$.ajax({
		url: 'lang/'+lang+'.php',
		dataType: 'json',
		success: function(html){
			$('.tx01').html(html.tx01);
			$('.tx02').html(html.tx02);
			$('.tx03').html(html.tx03);
			$('.tx04').html(html.tx04);
			$('.tx05').html(html.tx05);
			$('.tx06').html(html.tx06);
			$('.tx07').html(html.tx07);
			$('.tx08').html(html.tx08);
			$('.tx09').html(html.tx09);
			$('.tx10').html(html.tx10);
			$('.tx11').html(html.tx11);
			$('.tx12').html(html.tx12);
			$('.tx13').html(html.tx13);
			$('.tx14').html(html.tx14);
			$('.tx15').html(html.tx15);
			$('.tx16').html(html.tx16);
			$('.tx17').html(html.tx17);
			$('.tx18').html(html.tx18);
			$('.tx19').html(html.tx19);
			$('.tx20').html(html.tx20);
			$('.tx21').html(html.tx21);
			$('.tx22').html(html.tx22);
			$('.tx23').html(html.tx23);
			$('.tx24').html(html.tx24);
			$('.tx25').html(html.tx25);
			$('.tx26').html(html.tx26);
			$('.tx27').html(html.tx27);
			$('.tx29').html(html.tx29);
			$('.tx30').html(html.tx30);
			$('.tx31').html(html.tx31);
			$('.tx32').html(html.tx32);
			$('.tx33').html(html.tx33);
			$('.tx34').html(html.tx34);
			$('.tx35').html(html.tx35);
			$('.tx36').html(html.tx36);
			$('.tx37').html(html.tx37);
			$('.tx38').html(html.tx38);
			$('.tx39').html(html.tx39);
			$('.tx40').html(html.tx40);
			$('.tx41').html(html.tx41);
			$('.tx42').html(html.tx42);
			$('.tx43').html(html.tx43);
			$('.tx44').html(html.tx44);
			$('.tx45').html(html.tx45);
			$('.tx46').html(html.tx46);
			$('.tx47').html(html.tx47);
			$('.tx48').html(html.tx48);
			$('.tx49').html(html.tx49);
			$('.tx50').html(html.tx50);
			$('.tx51').html(html.tx51);
			$('.tx52').html(html.tx52);
			$('.tx53').html(html.tx53);
			$('.tx54').html(html.tx54);
			$('.tx55').html(html.tx55);
			$('.tx56').html(html.tx56);
			$('.tx57').html(html.tx57);
			$('.tx58').html(html.tx58);
			$('.tx59').html(html.tx59);
			$('.tx60').html(html.tx60);
			$('.tx61').html(html.tx61);
			$('.tx62').html(html.tx62);
			$('.tx63').html(html.tx63);
			$('.tx64').html(html.tx64);
			$('.tx65').html(html.tx65);
			$('.tx66').html(html.tx66);
			$('.tx67').html(html.tx67);
			$('.tx68').html(html.tx68);
			$('.tx69').html(html.tx69);
			$('.tx70').html(html.tx70);
			$('.tx71').html(html.tx71);
			$('.tx72').html(html.tx72);
			$('.tx73').html(html.tx73);
			$('.tx74').html(html.tx74);
			$('.tx75').html(html.tx75);
			$('.tx76').html(html.tx76);
			$('.tx77').html(html.tx77);
			$('.tx78').html(html.tx78);
			$('.tx79').html(html.tx79);
			$('.tx80').html(html.tx80);
			$('.tx81').html(html.tx81);
			$('.tx82').html(html.tx82);
			$('.tx83').html(html.tx83);
			$('.tx84').html(html.tx84);
			$('.tx85').html(html.tx85);
		}
	})
}
$('nav .languaje a').click(function (e) {
	e.preventDefault();
	$('.languaje a').removeClass('this-lang');
	$(this).addClass('this-lang');
	if($(this).hasClass('es')){
		lang = 'es';
	}
	if($(this).hasClass('en')){
		lang = 'en';
	}
	if($(this).hasClass('br')){
		lang = 'br';
	}
	_callAjax(lang)
});

$(document).bind('ready', function (e) {
	//fancybox
	$('.fancybox').fancybox();
});

$(document).bind('ready resize change', function (e) {
	//ejecutar la función para determinar la altura de la sección Inicio
	seccionInicio();
});

$(window).bind('scroll', function (e) {
	//fijar el menú luego de pasar la sección Inicio
	var scrollTop = $(window).scrollTop()
	var inicioHeight = $('#inicio').height()
	if (scrollTop >= inicioHeight) {
		if (!$('header').hasClass('fixed')) {
			$('header').addClass('fixed')
		}
	} else {
		if ($('header').hasClass('fixed')) {
			$('header').removeClass('fixed')
		}
	}
	//ejecutar la función para el funcionamiento del menú
	scrollWatch('body > div', 'nav')
});
