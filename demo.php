<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/fancybox.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <!--[if gte IE 9]>
          <style type="text/css">
            .gradient {
               filter:none;
            }
          </style>
        <![endif]-->
    </head>
    <body id="demo">
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="inicio" class="clearfix">
            <div class="image shadow-inner-bottom">
                <ul class="center t-center">
                    <li class="t-center">
                        <h1 class="center">uakika NFC</h1>
                    </li>
                </ul>    	
            </div>
        </div>
        <header class="shadow-outer-bottom bg-blue-light clearfix">
            <div class="wrapper center clearfix">
                <nav class="t-center clearfix">
                    <a href="#inicio" class="item tx02 current">Inicio</a>
                    <a href="#compra-de-producto" class="item tx03">Compra de producto</a>
                    <a href="#comentario-y-valoracion" class="item tx04">Comentario</a>
                    <a href="#comentario-y-valoracion" class="item tx04">Valoración</a>
                    <a href="#encuesta-y-suscripcion" class="item tx05">Encuesta</a>
                    <a href="#encuesta-y-suscripcion" class="item tx05">Suscripción</a>
                </nav>
            </div>
        </header>



        <div id="compra-de-producto" class="clearfix">
            <div class="wrapper center">
                <h1 class="t-center blue-light tx23">Datos de Cliente</h1>    
                <div class="border-box border-radius border-gray bg-gray clearfix">
                    <h2 class="blue-medium">Datos :</h2>
                    <p class="italic tx25">Cliente : andres@uakika.com	</p>
                    <label for="customerClientKey">Api Client Key: </label>
                    <input type="text" id="customerClientKey"  name="customerClientKey" value="aab3238922bcc25a6f606eb525ffdc56" size="40">            
                    <br>
                    <br>
                    <label for="fan_email">Fan Email</label>
                    <input type="text" id="customerFanUserEmail"  name="customerFanUserEmail" value="andres.kelly@gmail.com" size="34">            
                    <input type="submit" class="submit clear right shadow-outer border-radius bg-blue-gradient blue-lighter tx78" id="butSignin" value="Signin">
                    <div id="response"></div>
                </div>
            </div>
        </div>
        <div id="compra-de-producto" class="clearfix">
            <div class="wrapper center">
                <h1 class="t-center blue-light tx23">Compra de producto</h1>    
                <div class="border-box border-radius border-gray bg-gray clearfix">
                    <img class="shadow-outer border-white circle" src="img/bicicleta.gif">
                    <h2 class="blue-medium">Bicicleta Spider Playa</h2>
                    <input type="hidden" id="actionHashCompra"  name="actionHashCompra" value="55a1ba26e17fd93bfb97107efad4495e2fc6f29f">            
                    <div id="responseComprar"></div>
                    <p class="italic tx25">Bicicleta de paseo</p>
                    <p class="italic tx25">De dama, rodado 26. Colores disponibles según stock</p>
                    <p class="italic tx25">CARACTERÍSTICAS:</p>
                    <ul class="left lista-simple italic tx25"><li>Cuadro de acero</li><li>Horquilla: Rígida</li><li>Asiento con resortes</li></ul>
                    <input type="button" id="comprar" class="submit clear right shadow-outer border-radius bg-blue-gradient blue-lighter tx78" value="COMPRAR">
                </div>
            </div>
        </div>
        <div id="comentario-y-valoracion" class="bg-gray clearfix">
            <div class="wrapper center clearfix">
                <div class="left">
                    <h1 class="blue-light tx23 t-center">Comentario</h1>
                    <div class="border-box border-radius border-gray bg-white clearfix">
                        <h4 class="blue-medium tx24">¡Contanos qué estás pensando! </h4>
                        <div id="responseComentar"></div>
                        <input type="hidden" id="actionHashComentar"  name="actionHashComentar" value="9e36891ecb1051800c313b6b3df422f2701c1c2a">            
                        <textarea class="border-box border-radius border-gray bg-white"></textarea>
                        <input type="button" class="submit clear right shadow-outer border-radius bg-blue-gradient blue-lighter tx78" id="comentar" value="COMENTAR">
                    </div>

                </div>
                <div class="right">
                    <h1 class="blue-light tx23 t-center">Valoración</h1>
                    <div class="border-box border-radius border-gray bg-white clearfix">
                        <h4 class="blue-medium tx24">¿Cómo calificarías nuestros servicios?</h4>
                        <input type="hidden" id="actionHashValorar"  name="actionHashValorar" value="c6119af34adb698e43aee6a6da7ed785cf9922d6">            
                        <div id="responseValorar"></div>
                        <div class="rating">
                            <span class="valoracion" value="1">☆</span>
                            <span  class="valoracion" value="2">☆</span>
                            <span class="valoracion" value="3">☆</span>
                            <span class="valoracion" value="4">☆</span>
                            <span class="valoracion" value="5">☆</span>
                        </div>                
                    </div>
                </div>
            </div>
        </div>
        <div id="encuesta-y-suscripcion" class="clearfix">
            <div class="wrapper center clearfix">
                <div class="left">
                    <h1 class="blue-light tx23 t-center">Encuesta</h1>
                    <div class="border-box border-radius border-gray bg-white clearfix">
                        <h4 class="blue-medium tx24">¡Contanos qué estás pensando!</h4>
                        <input type="hidden" id="actionHashEncuesta"  name="actionHashEncuesta" value="b8febfe1128f96a26b2f5f7caeb5f1ecf1dbf3a7">            
                        <div id="responseEncuesta"></div>
                        <div class="clearfix field-wrapper checkboxes">
                            <input type="radio">
                            <label>Playa</label>
                            <input type="radio">
                            <label>Campo</label>
                            <input type="radio">
                            <label>Montaña</label>
                        </div>
                        <div class="clearfix field-wrapper select">
                            <label>¿Nos recomendaría?</label>
                            <select>
                                <option>Sí</option>
                                <option>No</option>
                                <option>NS/NC</option>
                            </select>
                        </div>
                        <input type="button" id="encuesta" class="submit clear right shadow-outer border-radius bg-blue-gradient blue-lighter tx78" value="VOTAR">
                    </div>
                </div>
                <div class="right">
                    <h1 class="blue-light tx23 t-center">Suscripción</h1>
                    <div class="border-box border-radius border-gray bg-white clearfix">
                        <p>Ingresá tu email y suscribite para recibir nuestro newsletter semanal.</p>
                        <input type="hidden" id="actionHashNewsletter"  name="actionHashNewsletter" value="5b71f828157b37cce74016efc6fe3edd5be68a94">            
                        <div id="responseNewsletter"></div>
                        <div class="field-wrapper"><input type="text" class="text border-box border-radius border-gray bg-white" name="email"></div>
                        <input type="button" id="newsletter" class="submit clear right shadow-outer border-radius bg-blue-gradient blue-lighter tx78" value="SUSCRIBIRME">
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-blue-lighter">
            <ul class="wrapper center t-center clearfix">
                <li class="left italic tx79">Copyright © 2014 Uakika - Todos los derechos reservados</li>
                <li class="right italic t-right">
                    <a href="terminos-de-uso.html" data-fancybox-type="iframe" class="fancybox tx80">Términos de uso</a>
                    •
                    <a href="privacidad.html" data-fancybox-type="iframe" class="fancybox tx81">Privacidad</a>
                </li>
                <li class="om">&nbsp;</li>
            </ul>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!--<script>window.jQuery || document.write('<script src="http://code.jquery.com/jquery-latest.js"><\/script>')</script>-->
        <script src="js/vendor/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="js/vendor/apprise-1.5.min.js"></script>
        <script src="js/plugins.js"></script>
        <!--<script src="js/main.js"></script>-->
        <script type="text/javascript" src="http://nfc.uakika.com/bundles/uakikanfcbackend/js/customApi.js"></script>
        <!--<script type="text/javascript" src="http://uak-nfc.dev/bundles/uakikanfcbackend/js/customApi.js"></script>-->

        <script src="js/scripts.js"></script>
        <script type="text/javascript" language="javascript">
            var uak_settings = {customer_key: 'aab3238922bcc25a6f606eb525ffdc56', position: {x: 'right', y: 'bottom'}};
            $(document).ready(function () {
                // <![CDATA[
                jQuery(function ($) {
                    var uak = document.createElement('script');
                    uak.type = 'text/javascript';
                    uak.async = true;
                    uak.src = 'http://nfc.uakika.com/bundles/uakikanfcbackend/js/uak.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(uak, s);
                });
                // ]]>
<?php
if ($accion === "enviado")
{
    ?>
                    apprise('Tu mensaje ha sido enviado exitosamente!');
    <?php
} if ($accion === "error")
{
    ?>
                    apprise('Ha habido un error al enviar tu mensaje! Por favor, int&eacute;ntalo nuevamente en unos minutos.');
<?php } ?>
            });
        </script>




        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <!--script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
        </script-->
    </body>
</html>